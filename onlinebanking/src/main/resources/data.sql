DROP TABLE IF EXISTS usersprofile;
DROP TABLE IF EXISTS savingsaccount;
commit;
CREATE TABLE usersprofile (
  userid INT AUTO_INCREMENT  PRIMARY KEY,
  password VARCHAR(250) NOT NULL,
  firstname VARCHAR(250) NOT NULL,
  lastname VARCHAR(250) NOT NULL,
  emailid VARCHAR(250) NOT NULL,
  mobilenumber VARCHAR(20) NOT NULL
);
commit; 
INSERT INTO usersprofile (password, firstname, lastname, emailid, mobilenumber) VALUES
  ('World@2019', 'Alex', 'Jones', 'alex.jones@gmail.com', '+447443726190'),
  ('World@2019', 'Laura', 'Pole', 'laura@gmail.com', '+447443726199'),
  ('World@2019', 'Anish', 'Joseph', 'ajoesph@gmail.com', '+447443726196'),
  ('World@2019', 'Ashok', 'Kumar', 'ashokkumar@gmail.com', '+447443726192'),
  ('World@2019', 'Mark', 'Pepper', 'mark.pepper.uk@gmail.com', '+447443726191');
  
commit;
CREATE TABLE savingsaccount (
  accountnumber INT AUTO_INCREMENT  PRIMARY KEY,
  userid INT NOT NULL,
  dateofopening datetime NOT NULL,
  accountCategory VARCHAR(250) NOT NULL,
  FOREIGN KEY (userid) REFERENCES usersprofile(userid)
);
commit;