package com.ing.onlinebanking.services;

import org.springframework.stereotype.Service;

import com.ing.onlinebanking.web.beans.WebSavingAccountRegDataModal;

/**
 * @author vinishvijayakumar
 *
 */
@Service
public interface SavingsAccountService {

	/**
	 * This method is used to verify whether the user owes savings account.
	 * @param emailId
	 * @return true if user has savings account.
	 */
	public boolean isUserHasSavingsAccount(String emailId);
	
	/**
	 * This method is used to validate where the user is eligible to register a savings account.
	 * @return list of errors if not eligible.
	 */
	public StringBuffer isUserEligibleToRegister(String emailId);
	
	/**
	 * This method is used to open savings account.
	 * @param data
	 * @return
	 */
	public Long openSavingsAccount(WebSavingAccountRegDataModal data);
	
}
