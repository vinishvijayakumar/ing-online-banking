package com.ing.onlinebanking.services.impl;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ing.onlinebanking.dao.entities.User;
import com.ing.onlinebanking.dao.repository.UserRepository;
import com.ing.onlinebanking.services.UserLoginService;
import com.ing.onlinebanking.utils.AppUtils;
import com.ing.onlinebanking.web.beans.WebLoginDataModal;
import com.ing.onlinebanking.web.constants.SessionConstants;

/**
 * @author vinishvijayakumar
 *
 */
@Component
public class UserLoginServiceImpl implements UserLoginService {
	
	private Logger log = LoggerFactory.getLogger(UserLoginServiceImpl.class);
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public boolean verifyUserAuthentication(WebLoginDataModal loginData, HttpSession session) {
		
		User user = userRepository.verifyUser(loginData.getUserName(), loginData.getUserPassword());
		
		if(AppUtils.isValidUserProfile(user)) {
			
			log.info("User authentication is verifed successfully.. for " + loginData.getUserName());
			
			session.setAttribute(SessionConstants.USER_EMAIL, user.getEmailId());
			return true;
		}
		
		log.error("User authentication is failed.. for " + loginData.getUserName());
		
		return false;
	}

	@Override
	public boolean isExistingUser(String emailId) {
		return AppUtils.isValidUserProfile(userRepository.findOne(emailId));
	}

}
