package com.ing.onlinebanking.services.impl;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ing.onlinebanking.dao.entities.SavingsAccount;
import com.ing.onlinebanking.dao.entities.User;
import com.ing.onlinebanking.dao.repository.SavingsAccountRepository;
import com.ing.onlinebanking.dao.repository.UserRepository;
import com.ing.onlinebanking.properties.ApplicationProperties;
import com.ing.onlinebanking.services.SavingsAccountService;
import com.ing.onlinebanking.services.UserLoginService;
import com.ing.onlinebanking.utils.AppUtils;
import com.ing.onlinebanking.web.beans.WebSavingAccountRegDataModal;
import com.ing.onlinebanking.web.constants.Constants;

/**
 * @author vinishvijayakumar
 *
 */
@Component
public class SavingsAccountServiceImpl implements SavingsAccountService {

	private Logger log = LoggerFactory.getLogger(SavingsAccountServiceImpl.class);
	
	@Autowired
	private SavingsAccountRepository savingsAccountRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserLoginService loginService;
	
	@Autowired
	private ApplicationProperties applicationProperties;
	
	@Override
	public boolean isUserHasSavingsAccount(String emailId) {
		
		User user = userRepository.findOne(emailId);
		
		if(AppUtils.isValidUserProfile(user)) {
			return savingsAccountRepository.findUser(user.getUserid()) != null;
		} else {
			log.error("User profile is not valid for - " + emailId);
		}
		
		return false;		
	}

	@Override
	public StringBuffer isUserEligibleToRegister(String userEmailId) {
		
		StringBuffer errors = new StringBuffer();
		
		if(userEmailId != null) {
			if(!loginService.isExistingUser(userEmailId)) {
				errors.append(applicationProperties.getSavingsActRegErrorMsgNewUser());
			} else if(isUserHasSavingsAccount(userEmailId)) {
				errors.append(applicationProperties.getSavingsActRegErrorMsgAccountExists());
			} else if(!isAllowedTime()) {
				errors.append(applicationProperties.getSavingsActRegErrorMsgInvalidTime());
			} else {
				// do nothing
			}
			
		} else {
			errors.append("Invalid data error: user email-id is not available.");
		}
		
		// return all errors
		return errors;
	}

	@Override
	public Long openSavingsAccount(WebSavingAccountRegDataModal data) {
		// get user info
		User user = userRepository.findOne(data.getEmailId());
		// validate use profile
		if(AppUtils.isValidUserProfile(user)) {
			// create repository object
			SavingsAccount savingsAccount = new SavingsAccount();
			savingsAccount.setAccountCategory(data.getAccountCategory());
			savingsAccount.setUserId(user.getUserid());
			savingsAccount.setDateOfOpening(new Date());
			// commit repository object
			savingsAccountRepository.save(savingsAccount);
			// Get account number
			return getSavingsAccountNumber(user.getUserid());
			
		} else {
			log.error("User profile is not valid for - " + data.getEmailId());
		}
		
		return null;
	}
	
	/**
	 * Verify allowed day and time
	 * @return true if allowed
	 */
	private boolean isAllowedTime() {
		
		// days
		List<String> allowedDaysList =Arrays.asList(applicationProperties.getSavingsActRegAllowedWeekdays().split(Constants.DELIMETER_COMMA));
		
		// times
		int allowedStartTime = Integer.parseInt(applicationProperties.getSavingsActRegAllowedStartTimeOfday());
		int allowedEndTime = Integer.parseInt(applicationProperties.getSavingsActRegAllowedEndTimeOfday());
		
		// remove duplicates if any
		Set<String> allowedDays = new HashSet<String>(allowedDaysList);
		
		LocalDateTime now = LocalDateTime.now();
		
		// manipulate times for validation
		LocalTime startTime = LocalTime.of(allowedStartTime, 0);
		LocalTime endime = LocalTime.of(allowedEndTime, 0);
		LocalTime txnTime = LocalTime.of(now.getHour(), now.getMinute());

		// validation rule
		return allowedDays.contains(now.getDayOfWeek().name()) && !txnTime.isBefore(startTime) && !txnTime.isAfter(endime);
	}
	
	/**
	 * Get savings account number of an user.
	 * @param userId
	 * @return savings account number
	 */
	private Long getSavingsAccountNumber(Long userId) {
		SavingsAccount savingsAccount = savingsAccountRepository.findUser(userId);
		return savingsAccount != null ? savingsAccount.getAccountnumber() : null;
	}

}
