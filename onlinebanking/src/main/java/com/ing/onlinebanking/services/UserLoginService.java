/**
 * User Services.
 */
package com.ing.onlinebanking.services;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.ing.onlinebanking.web.beans.WebLoginDataModal;

/**
 * @author vinishvijayakumar
 *
 */
@Service
public interface UserLoginService {

	/**
	 * This method is used to verify the user authentication to access the application.
	 * @param loginData
	 * @param session
	 * @return
	 */
	public boolean verifyUserAuthentication(WebLoginDataModal loginData, HttpSession session);
	
	/**
	 * This method is used to verify the email id owner is an existing user or not.
	 * @param emailId
	 * @return
	 */
	public boolean isExistingUser(String emailId);
	
}
