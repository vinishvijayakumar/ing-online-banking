/**
 * Application utilities methods.
 */
package com.ing.onlinebanking.utils;

import com.ing.onlinebanking.dao.entities.User;

/**
 * @author vinishvijayakumar
 *
 */
public class AppUtils {

	public static boolean isValidUserProfile(User user) {
		return user != null && user.getEmailId() != null && user.getMobileNumber() != null ? true : false;
	}
}
