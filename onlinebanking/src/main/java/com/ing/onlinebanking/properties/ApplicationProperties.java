package com.ing.onlinebanking.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author vinishvijayakumar
 *
 */
@Component
public class ApplicationProperties {

	@Value("${com.ing.onlinebanking.savingsAccount.registration.allowed.weekDays}")
	public String savingsActRegAllowedWeekdays;
	
	@Value("${com.ing.onlinebanking.savingsAccount.registration.allowed.startTimeofDay.AM}")
	public String savingsActRegAllowedStartTimeOfday;
	
	@Value("${com.ing.onlinebanking.savingsAccount.registration.allowed.esndTimeOfDay.PM}")
	public String savingsActRegAllowedEndTimeOfday;
	
	@Value("${com.ing.onlinebanking.savingsAccount.error.msg.invalidTime}")
	public String savingsActRegErrorMsgInvalidTime;
	
	@Value("${com.ing.onlinebanking.savingsAccount.error.msg.savingsAccountExists}")
	public String savingsActRegErrorMsgAccountExists;
	
	@Value("${com.ing.onlinebanking.savingsAccount.error.msg.notAnExistingUser}")
	public String savingsActRegErrorMsgNewUser;
	
	@Value("${com.ing.onlinebanking.savingsAccount.success.msg}")
	public String savingsActRegSuccessMsg;

	public String getSavingsActRegAllowedWeekdays() {
		return savingsActRegAllowedWeekdays;
	}

	public String getSavingsActRegAllowedStartTimeOfday() {
		return savingsActRegAllowedStartTimeOfday;
	}

	public String getSavingsActRegAllowedEndTimeOfday() {
		return savingsActRegAllowedEndTimeOfday;
	}

	public void setSavingsActRegAllowedWeekdays(String savingsActRegAllowedWeekdays) {
		this.savingsActRegAllowedWeekdays = savingsActRegAllowedWeekdays;
	}

	public void setSavingsActRegAllowedStartTimeOfday(String savingsActRegAllowedStartTimeOfday) {
		this.savingsActRegAllowedStartTimeOfday = savingsActRegAllowedStartTimeOfday;
	}

	public void setSavingsActRegAllowedEndTimeOfday(String savingsActRegAllowedEndTimeOfday) {
		this.savingsActRegAllowedEndTimeOfday = savingsActRegAllowedEndTimeOfday;
	}

	public String getSavingsActRegErrorMsgInvalidTime() {
		return savingsActRegErrorMsgInvalidTime;
	}

	public String getSavingsActRegErrorMsgAccountExists() {
		return savingsActRegErrorMsgAccountExists;
	}

	public String getSavingsActRegErrorMsgNewUser() {
		return savingsActRegErrorMsgNewUser;
	}

	public void setSavingsActRegErrorMsgInvalidTime(String savingsActRegErrorMsgInvalidTime) {
		this.savingsActRegErrorMsgInvalidTime = savingsActRegErrorMsgInvalidTime;
	}

	public void setSavingsActRegErrorMsgAccountExists(String savingsActRegErrorMsgAccountExists) {
		this.savingsActRegErrorMsgAccountExists = savingsActRegErrorMsgAccountExists;
	}

	public void setSavingsActRegErrorMsgNewUser(String savingsActRegErrorMsgNewUser) {
		this.savingsActRegErrorMsgNewUser = savingsActRegErrorMsgNewUser;
	}

	public String getSavingsActRegSuccessMsg() {
		return savingsActRegSuccessMsg;
	}

	public void setSavingsActRegSuccessMsg(String savingsActRegSuccessMsg) {
		this.savingsActRegSuccessMsg = savingsActRegSuccessMsg;
	}
}
