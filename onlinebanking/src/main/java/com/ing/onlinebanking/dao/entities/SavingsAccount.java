/**
 * Savings Account repository entity.
 */
package com.ing.onlinebanking.dao.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * @author vinishvijayakumar
 *
 */
@Entity
@Table(name = "savingsaccount")
@EntityListeners(AuditingEntityListener.class)
public class SavingsAccount implements Serializable {
	
	/**
	 * auto generated serial version ID.
	 */
	private static final long serialVersionUID = -7332491520410913903L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long accountnumber;
	
	@Column(name="userid", nullable = false)
	private Long userId;
	
	@Column(name="dateofopening", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date dateOfOpening;
	
	@Column(name="accountcategory", nullable = false)
	private String accountCategory;

	public Long getAccountnumber() {
		return accountnumber;
	}

	public Long getUserId() {
		return userId;
	}

	public Date getDateOfOpening() {
		return dateOfOpening;
	}

	public String getAccountCategory() {
		return accountCategory;
	}

	public void setAccountnumber(Long accountnumber) {
		this.accountnumber = accountnumber;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setDateOfOpening(Date dateOfOpening) {
		this.dateOfOpening = dateOfOpening;
	}

	public void setAccountCategory(String accountCategory) {
		this.accountCategory = accountCategory;
	}
	
	

}
