package com.ing.onlinebanking.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ing.onlinebanking.dao.entities.User;

/**
 * @author vinishvijayakumar
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long>{

	@Query("select u from User u where u.emailId = ?1")
	User findOne(String userName);
	
	@Query("select u from User u where u.emailId = ?1 and u.password = ?2")
	User verifyUser(String emailId, String password);

}
