/**
 * User repository entity.
 */
package com.ing.onlinebanking.dao.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * @author vinishvijayakumar
 *
 */
@Entity
@Table(name = "usersprofile")
@EntityListeners(AuditingEntityListener.class)
public class User implements Serializable {
	
	/**
	 * auto generated serial version ID.
	 */
	private static final long serialVersionUID = -7332491520410912703L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long userid;
	
	@Column(name="password", nullable = false)
	private String password;
	
	@Column(name="firstname", nullable = false)
	private String firstName;
	
	@Column(name="lastname", nullable = false)
	private String lastName;
	
	@Column(name="emailid", nullable = false)
	private String emailId;
	
	@Column(name="mobilenumber", nullable = false)
	private String mobileNumber;
	
	public Long getUserid() {
		return userid;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
