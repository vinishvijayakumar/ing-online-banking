package com.ing.onlinebanking.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ing.onlinebanking.dao.entities.SavingsAccount;

/**
 * @author vinishvijayakumar
 *
 */
@Repository
public interface SavingsAccountRepository extends JpaRepository<SavingsAccount, Long> {
	
	@Query("select u from SavingsAccount u where u.userId = ?1")
	SavingsAccount findUser(Long userId);

}
