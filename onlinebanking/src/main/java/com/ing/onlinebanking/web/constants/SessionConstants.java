/**
 * Session constants
 */
package com.ing.onlinebanking.web.constants;

/**
 * @author vinishvijayakumar
 *
 */
public interface SessionConstants {

	public static String USER_EMAIL = "email";
}
