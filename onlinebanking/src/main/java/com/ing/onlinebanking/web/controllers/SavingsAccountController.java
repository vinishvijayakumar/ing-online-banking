/**
 * Savings Account Controller.
 */
package com.ing.onlinebanking.web.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.ing.onlinebanking.properties.ApplicationProperties;
import com.ing.onlinebanking.services.SavingsAccountService;
import com.ing.onlinebanking.web.beans.WebSavingAccountRegDataModal;
import com.ing.onlinebanking.web.beans.WebSavingsAccountRegResponseDataModal;
import com.ing.onlinebanking.web.constants.Constants;
import com.ing.onlinebanking.web.constants.SessionConstants;
import com.ing.onlinebanking.web.constants.WebViewsProvider;

/**
 * @author vinishvijayakumar
 *
 */
@RestController
@RequestMapping(value = {"/savings-account"})
public class SavingsAccountController implements WebViewsProvider {

private Logger log = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private SavingsAccountService savingsAccountService;
	
	@Autowired
	private ApplicationProperties applicationProperties;
	
	/**
	 * get savings account web view
	 * @param model
	 * @return model and view
	 */
	@RequestMapping(value="/registration", method = RequestMethod.GET)
	public ModelAndView getSavingsAccountRegistrationView(Model model, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
		
		log.debug("Savings account registration request is accepted");
		
		ModelAndView modelAndView = new ModelAndView();
		
		String userEmailId = (String) httpServletRequest.getSession().getAttribute(SessionConstants.USER_EMAIL);
		
		StringBuffer validatiionResult = savingsAccountService.isUserEligibleToRegister(userEmailId);
		
		if(validatiionResult.length() == 0) {
			
			log.info("User is eligible to register savings account : " + userEmailId);
			
			modelAndView.setViewName(VIEW_SAVINGS_ACCOUNT_REGISTRATION);
			model.addAttribute("userEmail", userEmailId);
		} else {
			
			log.error(String.format("Error occurred while registering savings account : %s ; %s", userEmailId, validatiionResult));
			
			modelAndView.setViewName(VIEW_DASHBOARD);
			model.addAttribute("message", validatiionResult);
		} 
		
		
		return modelAndView;
	}
	
	/**
	 * process savings accounts request.
	 * @param model
	 * @return modal and view
	 */
	@RequestMapping(value="/registration", method = RequestMethod.POST)
	public ModelAndView registerSavingsAccount(Model model, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			@Valid @ModelAttribute WebSavingAccountRegDataModal webData, BindingResult result){
		
		log.debug("Savings account registration processing request is accepted");
		
		ModelAndView modelAndView = new ModelAndView();
		
		StringBuffer validatiionResult = savingsAccountService.isUserEligibleToRegister(webData.getEmailId());
		
		if(validatiionResult.length() == 0) {
			
			log.info("User is eligible to register savings account : " + webData.getEmailId());
			
			Long accountNumber = savingsAccountService.openSavingsAccount(webData);
			
			if(accountNumber != null && accountNumber.longValue() > 0) {
				
				log.info("User is successfully registered for savings account : " + webData.getEmailId());
				
				modelAndView.setViewName(VIEW_DASHBOARD);
				model.addAttribute("message", String.format(applicationProperties.getSavingsActRegSuccessMsg(), accountNumber));
			} else {
				
				log.error("Error occurred while registering savings account : " + webData.getEmailId());
				
				modelAndView.setViewName(VIEW_SAVINGS_ACCOUNT_REGISTRATION);
				model.addAttribute("message", "Error occurred while registering your details. Please try again.");
			}
			
		} else {
			
			log.error(String.format("Error occurred while registering savings account : %s ; %s", webData.getEmailId(), validatiionResult));
			
			modelAndView.setViewName(VIEW_SAVINGS_ACCOUNT_REGISTRATION);
			model.addAttribute("message", validatiionResult);
		} 
		
		model.addAttribute("userEmail", webData.getEmailId());
		return modelAndView;
	}
	
	/**
	 * process savings accounts API request
	 * @param model
	 * @return modal and view
	 */
	@RequestMapping(value="/api/registration", method = RequestMethod.POST)
	@ResponseBody
	public WebSavingsAccountRegResponseDataModal registerSavingsAccountApi(Model model, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			@Valid @RequestBody WebSavingAccountRegDataModal webData, BindingResult result){
		
		log.debug("Savings account registration request through API is accepted");
		
		WebSavingsAccountRegResponseDataModal response = new WebSavingsAccountRegResponseDataModal();
		
		StringBuffer validatiionResult = savingsAccountService.isUserEligibleToRegister(webData.getEmailId());
		
		if(validatiionResult.length() == 0) {
			
			log.info("User is eligible to register savings account : " + webData.getEmailId());
			
			Long accountNumber = savingsAccountService.openSavingsAccount(webData);
			
			if(accountNumber != null && accountNumber.longValue() > 0) {
				log.info("User is successfully registered for savings account : " + webData.getEmailId());
				response.setAccountNumnber(accountNumber);
				setSuccessResponse(response, String.format(applicationProperties.getSavingsActRegSuccessMsg(), accountNumber));
			} else {
				log.error("Error occurred while registering savings account : " + webData.getEmailId());
				setErrorResponse(response, new StringBuffer("Error occurred while registering your details. Please try again."));
			}
			
		} else {
			log.error(String.format("Error occurred while registering savings account : %s ; %s", webData.getEmailId(), validatiionResult));
			setErrorResponse(response, validatiionResult);
		}
		
		
		return response;
	}
	
	/**
	 * Set error response
	 * @param response
	 * @param message
	 */
	private void setErrorResponse(WebSavingsAccountRegResponseDataModal response, StringBuffer message) {
		response.setStatusCode(Constants.ERROR);
		response.setMessage(message.toString());
	}
	
	/**
	 * Set success response
	 * @param response
	 * @param message
	 */
	private void setSuccessResponse(WebSavingsAccountRegResponseDataModal response, String message) {
		response.setStatusCode(Constants.SUCCESS);
		response.setMessage(message.toString());
	}
	
}
