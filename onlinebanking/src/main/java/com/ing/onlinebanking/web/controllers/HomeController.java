/**
 * Application home controller.
 */
package com.ing.onlinebanking.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.ing.onlinebanking.web.constants.WebViewsProvider;

/**
 * @author vinishvijayakumar
 *
 */
@RestController
public class HomeController implements WebViewsProvider {

	private Logger log = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * get home page
	 * @param model
	 * @return
	 */
	@RequestMapping(value={"/", "/home"}, method = RequestMethod.GET)
	public ModelAndView  getHome(Model model){
		
		log.debug("Application home GET request is accepted....");
		
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(VIEW_HOME);
		return modelAndView;
	}
}
