/**
 * Application constants.
 */
package com.ing.onlinebanking.web.constants;

/**
 * @author vinishvijayakumar
 *
 */
public interface Constants {

	public static final String DELIMETER_COMMA = ","; 
	public static final String SUCCESS = "SUCCESS"; 
	public static final String ERROR = "ERROR"; 
}
