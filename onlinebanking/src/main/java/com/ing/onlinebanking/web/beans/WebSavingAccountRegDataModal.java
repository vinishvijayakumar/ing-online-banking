package com.ing.onlinebanking.web.beans;

import javax.validation.constraints.NotNull;

/**
 * @author vinishvijayakumar
 *
 */
public class WebSavingAccountRegDataModal {

	@NotNull
	private String emailId;
	
	@NotNull
	private String accountCategory;
	
	public String getEmailId() {
		return emailId;
	}
	public String getAccountCategory() {
		return accountCategory;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public void setAccountCategory(String accountCategory) {
		this.accountCategory = accountCategory;
	}
}
