package com.ing.onlinebanking.web.beans;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Vinish_Vijayakumar
 *
 */
public class WebLoginDataModal {

	@NotNull
	@Size(min = 5, max = 40, message = "User name is not valid.")
	private String userName;
	@NotNull
	@Size(min = 8,max = 15, message = "password is not valid.")
	private String userPassword;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}	
}
