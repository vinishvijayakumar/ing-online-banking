package com.ing.onlinebanking.web.beans;

/**
 * @author vinishvijayakumar
 *
 */
public class WebSavingsAccountRegResponseDataModal {

	private Long accountNumnber;
	private String statusCode;
	private String message;
	
	public Long getAccountNumnber() {
		return accountNumnber;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public String getMessage() {
		return message;
	}
	public void setAccountNumnber(Long accountNumnber) {
		this.accountNumnber = accountNumnber;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
