/**
 * List of web views
 */
package com.ing.onlinebanking.web.constants;

/**
 * @author vinishvijayakumar
 *
 */
public interface WebViewsProvider {

	public static String VIEW_HOME = "htmls/home";
	public static String VIEW_DASHBOARD = "htmls/dashboard";
	public static String VIEW_SAVINGS_ACCOUNT_REGISTRATION = "htmls/savingsactreg";
}
