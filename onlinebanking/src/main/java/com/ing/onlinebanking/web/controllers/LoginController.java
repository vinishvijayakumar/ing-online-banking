/**
 * User actions web controller.
 */
package com.ing.onlinebanking.web.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.ing.onlinebanking.services.UserLoginService;
import com.ing.onlinebanking.web.beans.WebLoginDataModal;
import com.ing.onlinebanking.web.constants.WebViewsProvider;

/**
 * @author vinishvijayakumar
 *
 */
@RestController
public class LoginController implements WebViewsProvider {


	private Logger log = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private UserLoginService loginService;
	
	/**
	 * process user login
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/userlogin", method = RequestMethod.POST)
	public ModelAndView login(Model model, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			@Valid @ModelAttribute WebLoginDataModal webLoginDataModal, BindingResult result){
		
		log.debug("User login request is accepted");
		
		ModelAndView modelAndView = new ModelAndView();
		
		if(loginService.verifyUserAuthentication(webLoginDataModal, httpServletRequest.getSession())) {
			
			log.info("User login is success for : " + webLoginDataModal.getUserName());
			
			modelAndView.setViewName(VIEW_DASHBOARD);
			model.addAttribute("userEmail", webLoginDataModal.getUserName());
		} else {
			
			log.error("User login is failed for : " + webLoginDataModal.getUserName());
			
			modelAndView.setViewName(VIEW_HOME);
			model.addAttribute("message", "You have entered invalid login details. Please try again.");
		}
		
		
		return modelAndView;
	}
}
