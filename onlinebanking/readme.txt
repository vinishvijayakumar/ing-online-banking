++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

How to run the project?
This project is developed using Spring Boot + Java 8
Import this project into STS and execute as Spring Boot App
Or Run maven and delpoy the war file into any webserver.

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Use below URL to open the in-memory H2 database on web:
>>>>>>>>>>>>>>> http://localhost:8099/ing/onlinebanking/in-memory-database 
Database url : jdbc:h2:mem:ingonlinebanking
User name: ingbank
password: 0n1!ne8ank

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

To access the web application use below URL:
>>>>>>>>>>>>>>> http://localhost:8099/ing/onlinebanking
Use credentials of users from in-memory database - usersprofile table to login (example: alex.jones@gmail.com / World@2019 )

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

To test the application using API :
Open any rest client and use below url.
>>>>>>>>>>>>>>> http://localhost:8099/ing/onlinebanking/savings-account/api/registration
request type: POST
request json structure:
{"emailId":"laura@gmail.com","accountCategory":"SAVINGS_ACCT_GOLD"}

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

